package com.yck.ocr_test

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.Task
import com.google.firebase.functions.FirebaseFunctions
import com.google.firebase.ktx.Firebase
import com.google.gson.*
import com.yck.ocr_test.databinding.ActivityCameraBinding
import java.io.ByteArrayOutputStream


private const val TAG = "MyCamera"

class MyCamera:AppCompatActivity(){

    val REQUEST_IMAGE_CAPTURE = 1
    var mImageBitmap:Bitmap?=null
    private lateinit var binding:ActivityCameraBinding
    private lateinit var functions: FirebaseFunctions

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCameraBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.btnCameraOn.setOnClickListener {
            dispatchTakePictureIntent()
        }

        //functions = FirebaseFunctions.getInstance()

        if(mImageBitmap!=null){

        }

    }

    //Take photo
    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }

    //Photo Result
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            val imageBitmap = data?.extras?.get("data") as Bitmap
            binding.ivCapture.setImageBitmap(imageBitmap)

            //이미지 사이즈 조정
            var scaleBitmap = scaleBitmapDown(imageBitmap, 640)

            //이미지Json 형태 변환
            var resultRequest:JsonObject = requestJSON(bitmapToBase64String(scaleBitmap).toString())

            //Vision API 전송(Text변환 시작)
            annotateImage(resultRequest.toString())
                .addOnCompleteListener { task ->
                    if (!task.isSuccessful) {
                        Log.d(TAG,"failed / exception  : ${task.exception.toString()}")
                        //Text변화 결과값 실패
                    } else {
                        //Text변화 결과값 성공
                        Log.d(TAG,"successful")
                    }
                }

        }
    }

    private fun requestJSON(base64encoded:String):JsonObject{
        // Create json request to cloud vision
        val request = JsonObject()
        // Add image to request
        val image = JsonObject()
        image.add("content", JsonPrimitive(base64encoded))
        request.add("image", image)
        //Add features to the request
        val feature = JsonObject()
        feature.add("type", JsonPrimitive("TEXT_DETECTION"))
        // Alternatively, for DOCUMENT_TEXT_DETECTION:
        // feature.add("type", JsonPrimitive("DOCUMENT_TEXT_DETECTION"))
        val features = JsonArray()
        features.add(feature)
        request.add("features", features)
        return request
    }

    //이미지 사이즈 최적크기로 변경
    fun scaleBitmapDown(bitmap: Bitmap, maxDimension: Int): Bitmap {
        //Device 가로 회전시 자동으로 수직 상태의 이미지로 변환 시켜줌 및 사이즈 조정해줌
        val originalWidth = bitmap.width
        val originalHeight = bitmap.height
        var resizedWidth = maxDimension
        var resizedHeight = maxDimension
        if (originalHeight > originalWidth) {
            resizedHeight = maxDimension
            resizedWidth =
                (resizedHeight * originalWidth.toFloat() / originalHeight.toFloat()).toInt()
        } else if (originalWidth > originalHeight) {
            resizedWidth = maxDimension
            resizedHeight =
                (resizedWidth * originalHeight.toFloat() / originalWidth.toFloat()).toInt()
        } else if (originalHeight == originalWidth) {
            resizedHeight = maxDimension
            resizedWidth = maxDimension
        }
        return Bitmap.createScaledBitmap(bitmap, resizedWidth, resizedHeight, false)
    }

    //Bitmap String 변환
    fun bitmapToBase64String(bitmap: Bitmap): String? {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
        val imageBytes: ByteArray = byteArrayOutputStream.toByteArray()
        return Base64.encodeToString(imageBytes, Base64.NO_WRAP)
    }

    fun annotateImage(requestJson: String): Task<JsonElement> {
        return functions
            .getHttpsCallable("annotateImage")
            .call(requestJson)
            .continueWith { task ->
                // This continuation runs on either success or failure, but if the task
                // has failed then result will throw an Exception which will be
                // propagated down.
                val result = task.result?.data
                JsonParser.parseString(Gson().toJson(result))
            }
    }



}//class end