package com.yck.ocr_test

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.Button
import androidx.core.content.FileProvider
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.yck.ocr_test.databinding.ActivityMainBinding
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


private const val TAG = "MainActivity"

class MainActivity : AppCompatActivity() {


    val REQUEST_IMAGE_CAPTURE = 1
    var currentPhotoPath: String = ""
    private var imageBitmap : Bitmap? = null
    lateinit var binding: ActivityMainBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //사진찍기(카메라 오픈)
        binding.captureImageButton.setOnClickListener {
            dispatchTakePictureIntent()
        }
        //이미지에서 텍스트 추출
        binding.detectTextButton.setOnClickListener {
            detectTextFromImage()
        }
    }

    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    return
                }
                // Continue only if the File was successfully created
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                        this,
                        "com.payamasefi.recognitiontest.fileprovider", //must be the same as manifest
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
                }
            }
        }
    }

    private fun createImageFile(): File {
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.KOREA).format(Date())
        val storageDir: File? = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            currentPhotoPath = absolutePath
        }
    }

    private fun galleryAddPic() {
        Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE).also { mediaScanIntent ->
            val f = File(currentPhotoPath)
            mediaScanIntent.data = Uri.fromFile(f)
            sendBroadcast(mediaScanIntent)
        }
    }

    private fun setPic() {
        // Get the dimensions of the View
        val targetW: Int = binding.photoContainer.width
        val targetH: Int = binding.photoContainer.height

        val bmOptions = BitmapFactory.Options().apply {
            // Get the dimensions of the bitmap
            inJustDecodeBounds = true

            BitmapFactory.decodeFile(currentPhotoPath, this)

            val photoW: Int = outWidth
            val photoH: Int = outHeight

            var scaleFactor: Int = 0

            // Determine how much to scale down the image
            try{
                scaleFactor = Math.max(1, Math.min(photoW / targetW, photoH / targetH))
                Log.d("test","scaleFactor : ${scaleFactor}")
            }catch (e:Exception){
                Log.d("test","${e.message.toString()}")
                scaleFactor = 4
            }

            // Decode the image file into a Bitmap sized to fill the View
            inJustDecodeBounds = false
            inSampleSize = scaleFactor
            inPurgeable = true
        }
        BitmapFactory.decodeFile(currentPhotoPath, bmOptions)?.also { bitmap ->
            imageBitmap = bitmap
            binding.photoContainer.setImageBitmap(imageBitmap)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            galleryAddPic() // If we want to save the picture
            setPic()
        }
    }

    private fun detectTextFromImage() {
        val firebaseVisionImage = FirebaseVisionImage.fromBitmap(imageBitmap!!)
        val firebaseVisionTextDetector = FirebaseVision.getInstance().cloudDocumentTextRecognizer
        firebaseVisionTextDetector.processImage(firebaseVisionImage).addOnSuccessListener {
            val blocks = it.blocks
            if (blocks.isNotEmpty()) {
                val stringBuffer = StringBuffer()
                blocks.forEach { bloc ->
                    stringBuffer.append(bloc.text)
                }
                Log.d(TAG,"stringbuffer:${stringBuffer}")
                binding.recognizedText.text = stringBuffer
            }

        }.addOnFailureListener {
            Log.d("p4yam", it.toString())
        }
    }



}//class end